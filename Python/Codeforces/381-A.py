'''input
7
1 2 3 4 5 6 7
'''

n = input()
arr = []
arr = map(int, raw_input().split())
lft = 0
rgt = len(arr) - 1
sum1 = 0
sum2 = 0

for i in range (n):
	#print str(i) + ' ' + str(sum1) + ' ' + str(sum2)
	if(arr[lft] > arr[rgt]):
		if(i & 1): sum2 += arr[lft]
		else: sum1 += arr[lft]

		lft += 1
	else:
		if(i & 1): sum2 += arr[rgt]
		else: sum1 += arr[rgt]

		rgt -= 1
print str(sum1) + ' ' + str(sum2)

