'''input
5 1 3 4
'''

arr = []
arr = map(int, raw_input().split())

pos256 = min(arr[0],arr[2],arr[3])

arr[0] -= pos256

ans = 256 * pos256

pos32 =  min(arr[0],arr[1])

ans += 32 * pos32

print ans