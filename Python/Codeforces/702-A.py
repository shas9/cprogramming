'''input
5
1 7 2 11 15
'''

ans = 0
n = int(raw_input())
arr = map(int,raw_input().split())
prev = -1
cnt = 0
for i in range (n):
	if(arr[i] > prev): cnt += 1
	else: cnt = 1

	prev = arr[i]
	ans = max(ans, cnt)

print ans