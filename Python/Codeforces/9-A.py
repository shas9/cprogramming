'''input
4 2
'''

import math

def gcd(a,b):
	if(b == 0): return a
	else: return gcd(b,a%b)  


arr = []
arr = map(int, raw_input().split())
mx = max(arr[0],arr[1])
baki = 6 - mx + 1
niche = 6
g = gcd(baki, 6)
baki /= g
niche /= g
print str(baki) + '/' + str(niche)