'''input
ares
'''

def fnd(a):
	return ord(a) - ord('a')

inp = raw_input()
last = 0
ans = 0

for i in range (len(inp)):
	curr = fnd(inp[i])
	diff = 0
	if(last < curr): diff = min(curr - last, 25 - curr + last + 1)
	else: diff = min(last - curr, 25 - last + curr + 1)
	ans += diff
	#print str(diff) + " " + str(ans) + " " + str(curr) + " " + inp[i] 
	last = curr
print ans
