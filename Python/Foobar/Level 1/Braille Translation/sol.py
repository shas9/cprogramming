def conv(s):

    if(s == ' '): return "000000"

    asccii = ord(s)
    s = s.lower()
    
    ret = ""
    if(asccii < 97): ret += "000001"

    if(s == 'a'): ret += "100000"
    elif(s == 'b'): ret += "110000"
    elif(s == 'c'): ret += "100100"
    elif(s == 'd'): ret += "100110"
    elif(s == 'e'): ret += "100010"
    elif(s == 'f'): ret += "110100"
    elif(s == 'g'): ret += "110110"
    elif(s == 'h'): ret += "110010"
    elif(s == 'i'): ret += "010100"
    elif(s == 'j'): ret += "010110"
    elif(s == 'k'): ret += "101000"
    elif(s == 'l'): ret += "111000"
    elif(s == 'm'): ret += "101100"
    elif(s == 'n'): ret += "101110"
    elif(s == 'o'): ret += "101010"
    elif(s == 'p'): ret += "111100"
    elif(s == 'q'): ret += "111110"
    elif(s == 'r'): ret += "111010"
    elif(s == 's'): ret += "011100"
    elif(s == 't'): ret += "011110"
    elif(s == 'u'): ret += "101001"
    elif(s == 'v'): ret += "111001"
    elif(s == 'w'): ret += "010111"
    elif(s == 'x'): ret += "101101"
    elif(s == 'y'): ret += "101111"
    elif(s == 'z'): ret += "101011"
    else: assert(0)

    return ret

 
def solution(s):
    
    ret = ""

    for i in range (len(s)):
    	ret += conv(s[i])

    return ret