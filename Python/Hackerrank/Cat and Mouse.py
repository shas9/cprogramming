'''input
2
1 2 3
1 3 2
'''

testcase = int(raw_input());


for i in range (testcase):
	x, y, z = map(int, raw_input().split())
	if(abs(x - z) == abs(y - z)): 
		print 'Mouse C'
    elif(abs(x - z) < abs(y - z)): 
    	print 'Cat A'
    else: 
    	print 'Cat B'