'''input
10 2 3
3 1
5 2 8
'''

b, n, m = map(int, raw_input().split())
keyboard = map(int, raw_input().split())
usb = map(int, raw_input().split())

ans = -1

for i in range (n):
	for j in range(m):
		if(keyboard[i] + usb[j] <= b): ans = max(ans, keyboard[i] + usb[j])

print ans