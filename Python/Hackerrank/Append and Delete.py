'''input
hi
hoo
3

'''

s = raw_input()
t = raw_input()
k = int(raw_input())

poss = 0
pref = 1

for i in range (min(len(s),len(t))):
	if(s[i] != t[i]): 
		need = len(s) - i + len(t) - i
		if(need <= k and ((k - need) % 2 == 0)): poss = 1
		pref = 0

if(pref): 
	if(abs(len(s) - len(t)) <= k and ((k - abs(len(s) - len(t))) % 2 == 0)): poss = 1

if(len(s) + len(t) <= k): poss = 1

if(poss): print "Yes"
else: print "No"

