'''input
0 3 4 2
'''

x1, v1, x2, v2 = map(int, raw_input().split())

if(v1 > v2): 
	if((x2 - x1) % (v1 - v2) == 0): print "YES"
	else: print "NO"
else: print "NO" 