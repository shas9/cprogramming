'''input
6 3
1 3 2 6 1 2
'''

n, m = map(int, raw_input().split())
arr = map(int, raw_input().split())
ans = 0

for i in range(n):
	for j in range(i + 1,n):
		sum = arr[i] + arr[j]
		if(sum % m == 0): ans += 1

print ans