'''input
2 3
2 4
16 32 96
'''


def _gcd(a, b):
	if(a == 0): return b
	else: return _gcd(b % a, a) 


def main():
	n, m = map(int, raw_input().split())
	arr1 = map(int, raw_input().split())
	arr2 = map(int, raw_input().split())

	gcd = arr2[0]
	lcm = arr1[0]

	for i in range(n):
		lcm = (lcm * arr1[i]) / _gcd(lcm, arr1[i])

	for i in range(m):
		gcd = _gcd(gcd, arr2[i])

	ans = 0
	for i in range(1, gcd + 1):
		if(gcd % i == 0):
			if(i % lcm == 0):
				ans += 1

	print ans


main()