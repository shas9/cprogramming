'''input
6
1073741825 1073741828 2
12345 1000000000000000000 1000
1000000000000 1000000000000000000 8
1 5 2
5 7 4
2 4 2
'''

testcase = int(raw_input())

for i in range(testcase):
	a, b, c = map(int, raw_input().split())
	ans = -1
	num = 1

	for j in range (65):
		if(num > b): break
		lft = a / num
		if(a % num): lft += 1
		if(a <= lft * num and lft * num <= b): ans = j
		num *= c
	
	assert(ans != -1)
	print ans