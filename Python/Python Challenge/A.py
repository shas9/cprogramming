'''input
20 23 6
'''

l, r, k = map(int, raw_input().split())

ans = 0
for i in range (l,r + 1):
	x = i;
	y = 0;
	
	while(x):
		y = (y * 10) + (x % 10)
		x = x / 10
	
	x = i
	diff = abs(x - y)

	if(diff % k == 0): ans += 1

print ans
