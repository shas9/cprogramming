<?php
echo
'<nav class="navbar navbar-expand-md navbar-custom fixed-top ">
<a class="navbar-brand logo-design-custom" href="home.html"> <img src="Image/sysmark square.png"  style="width:35px;height:30px;"> Sysmark</a>

<button class="navbar-toggler navbar-dark" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
  <span class="navbar-toggler-icon"></span>
</button>

<div class="collapse navbar-collapse" id="navbarSupportedContent">
  <ul class="navbar-nav mr-auto">

    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle color-me" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Hospital Bed
      </a>
      <div class="dropdown-menu dropbutton-design" aria-labelledby="navbarDropdown">
        <a class="dropdown-item dropbutton-design-menu" href="#">Electrical Bed</a>
        <a class="dropdown-item dropbutton-design-menu" href="#">Mechanical Bed</a>
        <div class="dropdown-divider"></div>
        <a class="dropdown-item dropbutton-design-menu" href="#">Others</a>
      </div>
    </li>
    
    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle color-me nav-bar-special" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Covid Special
      </a>
      <div class="dropdown-menu dropbutton-design" aria-labelledby="navbarDropdown">
        <a class="dropdown-item dropbutton-design-menu" href="#">Mask</a>
        <a class="dropdown-item dropbutton-design-menu" href="#">Medical Cap</a>
        <div class="dropdown-divider"></div>
        <a class="dropdown-item dropbutton-design-menu" href="#">Others</a>
      </div>
    </li>
    
    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle color-me" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Machine
      </a>
      <div class="dropdown-menu dropbutton-design" aria-labelledby="navbarDropdown">
        <a class="dropdown-item dropbutton-design-menu" href="#">X-ray Matchine</a>
        <a class="dropdown-item dropbutton-design-menu" href="#">ECG Machine</a>
        <div class="dropdown-divider"></div>
        <a class="dropdown-item dropbutton-design-menu" href="#">MRI Machine</a>
      </div>
    </li>
    
    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle color-me" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Baby Corner
      </a>
      <div class="dropdown-menu dropbutton-design" aria-labelledby="navbarDropdown">
        <a class="dropdown-item dropbutton-design-menu" href="#">Action</a>
        <a class="dropdown-item dropbutton-design-menu" href="#">Another action</a>
        <div class="dropdown-divider"></div>
        <a class="dropdown-item dropbutton-design-menu" href="#">Something else here</a>
      </div>
    </li>
    
    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle color-me" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Others
      </a>
      <div class="dropdown-menu dropbutton-design" aria-labelledby="navbarDropdown">
        <a class="dropdown-item dropbutton-design-menu" href="#">Action</a>
        <a class="dropdown-item dropbutton-design-menu" href="#">Another action</a>
        <div class="dropdown-divider"></div>
        <a class="dropdown-item dropbutton-design-menu" href="#">Something else here</a>
      </div>
    </li>
    
  </ul>

  <form class="form-inline my-2 my-lg-0">
    <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
    <button class="btn btn-outline-custom my-2 my-sm-0" type="submit">Search</button>
  </form>


  <button class = "btn"> <a href="#" data-toggle="tooltip" title="Add to your cart"><i class = "fa fa-cart-plus design-custom cart-design"></i></a></button>
  <button class = "btn"> <a href="#" data-toggle="tooltip" title="Sign-Up / Sign-In"><i class = "fa fa-user design-custom"> User</i></a></button>

</div>
</nav>';
?>
