clc;
clear all;
close all;

disp('linear convolution program');

x=input('Please Enter i/p x(n): [format: {x1, x2, x3] =');
m=length(x);
h=input('Please Enter i/p h(n): [format: {h1, h2, h3] =');
n=length(h);

x=[x,zeros(1,n)];
subplot(2,2,1), stem(x);

title('Here is i/p sequencce of x(n):');
xlabel('---->n');
ylabel('---->x(n)');
grid;

h=[h,zeros(1,m)];
subplot(2,2,2), stem(h);

title('Here is i/p sequencce of h(n):');
xlabel('---->n');
ylabel('---->h(n)');
grid;

disp('convolution of x(n) and h(n) is y(n):');

y=zeros(1,m+n-1);

for i=1:m+n-1
  y(i)=0;
  
  for j=1:m+n-1
    
    if(j<i+1)
    y(i)=y(i)+x(j)*h(i-j+1);
  end
  
end

end

y
subplot(2,2,[3,4]),stem(y),'r';

title('convolution of x(n) & h(n) is :');
xlabel('---->n');
ylabel('---->y(n)');
grid;