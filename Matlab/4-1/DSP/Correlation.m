clc;
clear all;
close all;

disp('Correlation Matlab Simulation');

z=input('Please Enter i/p h(n):');
m=length(z);
h=input('Please Enter i/p x(n):');
n=length(h);

x=fliplr(z);
x=[x,zeros(1,n)];
t=conv(h,x)
subplot(2,2,1), stem(x);

title('Here is the i/p sequence of x(n):');
xlabel('---->n');
ylabel('---->x(n)');
grid;

h=[h,zeros(1,m)];
subplot(2,2,2), stem(h);

title('Here is the i/p sequence of h(n):');
xlabel('---->n');
ylabel('---->h(n)');
grid;

disp('Correlation of x(n) and h(n) = y(n):');
y=zeros(1,m+n-1);

for i=1:m+n-1
  y(i)=0;
  for j=1:m+n-1
    if(j<i+1)
    y(i)=y(i)+x(j)*h(i-j+1);
  end
end
end

y
subplot(2,2,[3,4]),stem(y),'r';

title('Correlation of x(n) and h(n) is:');
xlabel('---->n');
ylabel('---->y(n)');
grid;