#include <iostream>
#include <cstdio>
#include <stack>

using namespace std;

int main()
{
    stack < char > samin;

    samin.push( 'M' );
    samin.push( 'A' );
    samin.push( 'S' );

    while( !samin.empty () )
    {
        cout << samin.top() << endl;
        samin.pop();
    }

    return 0;
}
