#include <bits/stdc++.h>

using namespace std;

void scanarray(int array[], int size)
{
	for(int i = 0; i < size; i++)
	{
		cin >> array[i];
	}
}
void printarray(int array[], int size)
{
	for(int i = 0; i < size; i++)
	{
		cout << "Element no " << i + 1 << ": " << array[i] << endl;
	}
}
void merge(int array[], int start, int mid, int end)
{
	int array1size = (mid - start + 1);
	int array2size = (end - mid);

	/*int left[array1size];
	int right[array2size];

	int i, j, k, l, m;

	for(i = 0; i < array1size; i++)
	{
		left[i] = array[start + i];
	}

	for(i = 0; i < array2size; i++)
	{
		right[i] = array[mid + 1 + i];
	}

	i = 0;
	j = 0;
	k = start;

	while(i < array1size && j < array2size)
	{
		if(left[i] <= right[j])
		{
			array[k] = left[i];
			k++;
			i++;
		}
		else
		{
			array[k] = right[j];
			k++;
			j++;
		}
	}
	//cout << "----" << endl;

	//if(j == array2size)
	while(i < array1size)
	{
		array[k] = left[i];
		k++;
		i++;
	}

    //(i == array1size)
	while(j < array2size)
	{
		array[k] = right[j];
		k++;
		j++;
	}*/

	//printarray(array, end);
	//printf("\n");

	int totalsize;

	totalsize = (end - start + 1);

	int temp[totalsize];

	int i, j, k, l;

	k = 0;
	i = start;
	j = mid + 1;

	while((i < (array1size + start)) && (j <= end))
	{
		if(array[i] <= array[j])
		{
			temp[k] = array[i];
			i++;
			k++;
		}
		else
		{
			temp[k] = array[j];
			j++;
			k++;
		}
	}

	while(i < (array1size + start))
	{
		temp[k] = array[i];
		i++;
		k++;
	}
	while(j <= end)
	{
		temp[k] = array[j];
		j++;
		k++;
	}

	for(i = 0, j = start; i < totalsize; i++, j++)
	{
		array[j] = temp[i];
	}

	return;
}
void mergesort(int array[], int start, int end)
{
	if(start < end)
	{
		int mid;

		cout << "end: " << end << " start: " << start << endl;
		mid = (end + start) / 2;
		cout << "mid: " << mid << endl;

		mergesort(array, start, mid);
		mergesort(array, mid + 1, end);
		merge(array, start, mid, end);
	}
	else
    {
        return;
    }


}

int main()
{
    int size;
	cin >> size;

	int array[size];

	scanarray(array, size);

	mergesort(array, 0, size - 1);

	printarray(array, size);

	return 0;
}
