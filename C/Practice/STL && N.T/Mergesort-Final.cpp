#include <bits/stdc++.h>

using namespace std;

void scanarray(int array[], int size)
{
	for(int i = 0; i < size; i++)
	{
		cin >> array[i];
	}
}
void printarray(int array[], int size)
{
	for(int i = 0; i < size; i++)
	{
		cout << "Element no " << i + 1 << ": " << array[i] << endl;
	}
}

void merge(int array[], int start, int mid, int end)
{
	int arraysize1 = mid - start + 1;
	int arraysize2 = end - mid;

	int left[arraysize1];
	int right[arraysize2];

	int i, j, k, l;

	for(i = 0; i < arraysize1; i++)
	{
		left[i] = array[start + i];
	}
	for(j = 0; j < arraysize2; j++)
	{
		right[j] = array[mid + 1 + j];
	}

	i = 0;
	j = 0;
	k = start;

	while(i < arraysize1 && j < arraysize2)
	{
		if(left[i] <= right[j])
		{
			array[k] = left[i];
			k++;
			i++;
		}
		else
		{
			array[k] = right[j];
			k++;
			j++
		}
	}

	while(i < arraysize1)
	{
		array[k] = left[i];
		k++;
		i++;
	}

	while(j < arraysize2)
	{
		array[k] = right[j];
		k++;
		j++;
	}
}

void mergesort(int array[], int start, int end)
{
	if(start == end)
	{
		return;
	}
	else
	{
		int mid = (start + end) / 2;

		mergesort(array, start, mid);
		mergesort(array, mid + 1, end);
		merge(array, start, mid, end);
	}
}

int main()
{
	int size;
	cin >> size;
	int array[size];

	scanarray(array, size);

	mergesort(array, 0, size - 1);

	printarray(array, size);

	return 0;
}