#include <iostream>
#include <cstdio>
#include <vector>

using namespace std;

int main()
{
    string a, b, c, d;

    vector < string > strv;

    a = "AMI PAGOL ";
    b = a;
    c = "TUMI KI JANO?";
    d = a + b + c;

    printf("with printf = %s\n", d.c_str());

    cout << d.size() << endl;

    cout << d << endl;

    return 0;
}
