#include <stdio.h>

int main()
{
    int size;

    scanf("%d", &size);

    int x[size];

    int i, j, min;

    for(i = 0; i < size; i++)
    {
        scanf("%d", &x[i]);
    }

    for(j = 0; j < size; j++)
    {
        min = x[0];
        for(i = 1; i < size; i++)
        {
            if(min < x[i])
            {
                x[i - 1] = x[i];
                x[i] = min;
            }
            else
            {
                min = x[i];
            }
        }
    }
    printf("\n");
    for(i = 0; i < size; i++)
    {
        printf("%d ", x[i]);
    }

    return 0;
}
