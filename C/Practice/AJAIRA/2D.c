#include <stdio.h>

int main()
{
    int row, col;

    scanf("%d%d", &row, &col);

    int x[row][col];

    int i, j;

    for(i = 0; i < row; i++)
    {
        for(j = 0; j < col; j++)
        {
            scanf("%d", &x[i][j]);
        }

    }

    printf("\n");

    for(i = 0; i < row; i++)
    {

        for(j = 0; j < col; j++)
        {
            printf("%d ", x[i][j]);
        }
        printf("\n");
    }

}
