#include <stdio.h>

int main()
{
    int i, j;

    for(i = 1; i < 5; i++)
    {
        printf("\ni = %d", i);
        for(j = 0; j <= (i / 2); j++)
        {
            printf("\nj = %d", i + j);
        }
    }

    return 0;
}
