#include <stdio.h>

int main()
{
    int row, col, h;

    scanf("%d%d%d", &h, &row, &col);

    int x[h][row][col];

    int i, j, k;


    for(i = 0; i < h; i++)
    {
        for(j = 0; j < row; j++)
        {
            for(k = 0; k < col; k++)
            {
                scanf("%d", &x[i][j][k]);
            }
        }
        //printf("\n");
    }

    for(i = 0; i < h; i++)
    {
        for(j = 0; j < row; j++)
        {
            for(k = 0; k < col; k++)
            {
                printf("%d ", x[i][j][k]);
            }
            printf("\n");
        }
        printf("\n");
    }
    return 0;
}

