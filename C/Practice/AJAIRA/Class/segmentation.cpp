#include <bits/stdc++.h>

using namespace std;

#define ll long long

int main()
{
	/*

	ll segments[5] = {1,2,3,4,5};

	*/
	ll base[5] = {1000,2000,3000,4000,5000};
	ll limit[5] = {100,200,300,400,500};

//	for(ll i = 0; i < 5; i++) cin >> base[i];
//	for(ll i = 0; i < 5; i++) cin >> limit[i];

	for(ll i = 0; i < 5; i++)
	{
		cout << "Segment No: " << i << "\n Range:" << base[i] << " to " << base[i] + limit[i] << endl;
	}

	ll seg, offset;

	cout << "Enter Segment Number: ";
	cin >> seg;

	cout << "Enter Offset Number:";
	cin >> offset;

	if(offset < limit[seg])
	{
		cout << base[seg] + offset << "\n";
	}
	else
	{
		cout << "Limit Exceeded\n";
	}



}
