#include<stdio.h>
int main()
{
    int i,j,k,minimum,rs[25],m[10],cnt[10],flag[25],n,f,pf=0,next=1;

    printf("Enter the length of reference string: ");
    scanf("%d",&n);
    printf("Enter the reference string: ");
    for(i=0;i<n;i++)
    {
        scanf("%d",&rs[i]);
        flag[i]=0;
    }
    printf("Enter the number of frames: ");
    scanf("%d",&f);
    for(i=0;i<f;i++)
    {
        cnt[i]=0;
        m[i]=1;
    }
    printf("\nThe Page Replacement Process: \n");
    for(i=0;i<n;i++)
    {
        for(j=0;j<f;j++)
        {
            if(m[j]==rs[i])
            {
                flag[i]=1;
                cnt[j]=next;
                next++;
            }
        }
        if (flag[i]==0)
        {
            if(i<f)
            {
                m[i]=rs[i];
                cnt[i]=next;
                next++;
            }
            else
                {
                    minimum=0;
                    for(j=1;j<f;j++)
                        if(cnt[minimum]>cnt[j])
                        minimum=j;
                    m[minimum]=rs[i];
                    cnt[minimum]=next;
                    next++;

            }
            pf++;
        }
        for(j=0;j<f;j++)
            printf("%d\t",m[j]);
        if(flag[i]==0)
            printf("PF No. :%d",pf);
        printf("\n");
    }
    printf("\nThe number of page faults using LRU are %d",pf);
}
