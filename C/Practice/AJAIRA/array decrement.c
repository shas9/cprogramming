#include <stdio.h>

int main()
{
    int size;

    scanf("%d", &size);

    int x[size];

    int i, max, j;

    for(i = 0; i < size; i++)
    {
        scanf("%d", &x[i]);
    }

    for(j = 0; j < size; j++)
    {
        max = x[0];

    for(i = 1; i < size; i++)
    {
        if(x[i] < max)
        {
            x[i - 1] = x[i];
            x[i] = max;
        }
        else
        {
            max = x[i];
        }
    }
    }


    printf("\n");
    for(i = 0; i < size; i++)
    {
        printf("%d ", x[i]);
    }

    return 0;
}
