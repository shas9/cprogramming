 // Lights will guide you home

#include <bits/stdc++.h>

#define slld(longvalue) scanf("%lld", &longvalue)

#define ll long long
#define pll pair < ll, ll >

#define fastio ios_base:: sync_with_stdio(false); cin.tie(0); cout.tie(0)

#define bug(x) printf("BUG %lld\n", x)
#define pb push_back

#define mxlld LLONG_MAX
#define mnlld -LLONG_MAX

using namespace std;

bool check(ll n, ll pos)
{
	return n & (1LL << pos);
}

ll Set(ll n, ll pos)
{
	return n = n | (1LL << pos);
}

int main()
{
    ll i, j, k, l, m, n, r, q;
    ll testcase;
    ll in, ans;

    #ifdef Has9

        freopen("input.txt", "r", stdin);
        freopen("output.txt", "w", stdout);

    #endif

    while(cin >> n >> m)
    {
        ll d, s;
        cin >> d >> s;

        ll baki = (d * n - m * s);
        double ans = (baki * 1.0 / (n - m));

        if((baki > (100 * (n - m))) || (baki < 0))
        {
            cout << "impossible\n";
            continue;
        }

        cout << setprecision(10) << fixed << ans << "\n";

    }
}
