 // Lights will guide you home

#include <bits/stdc++.h>

#define slld(longvalue) scanf("%lld", &longvalue)

#define ll long long
#define pll pair < ll, ll >

#define fastio ios_base:: sync_with_stdio(false); cin.tie(0); cout.tie(0)

#define bug(x) printf("BUG %lld\n", x)
#define pb push_back

#define mxlld LLONG_MAX
#define mnlld -LLONG_MAX

using namespace std;

bool check(ll n, ll pos)
{
	return n & (1LL << pos);
}

ll Set(ll n, ll pos)
{
	return n = n | (1LL << pos);
}

ll dp[10005];
ll arr[102];

ll solve(ll k, ll n)
{
    if(k <= n) return arr[k];

    ll &ret = dp[k];

    if(ret != -1) return ret;

    ret = mxlld;

    for(ll i = 1; i <= n; i++)
    {
        ret = min(ret, solve(k - i, n) + arr[i]);
    }

    return ret;
}

int main()
{
    ll i, j, k, l, m, n, r, q;
    ll testcase;
    ll in, ans;

    #ifdef Has9

        freopen("input.txt", "r", stdin);
        freopen("output.txt", "w", stdout);

    #endif

    while(cin >> n >> m)
    {
        long double mn = 1e15;
        ll pos = -1;
        ll value = 0;

        for(ll i = 1; i <= n; i++) 
        {
            cin >> arr[i];

            long double x = (arr[i] / (double)i);

            if(mn > x)
            {
                mn = x;
                value = arr[i];
                pos = i;
            }
        }

        assert(pos != -1);

        memset(dp,-1,sizeof dp);

        for(ll i = 1; i <= m; ++i)
        {
            cin >> k;

            ll need = max(0LL, k - n - 500) / pos;

            ans = need * value;

            k -= (need * pos);

            ans += solve(k,n);

            cout << ans << "\n";
        }

    }
}
