 // Lights will guide you home

#include <bits/stdc++.h>

#define slld(longvalue) scanf("%lld", &longvalue)

#define ll long long
#define pll pair < ll, ll >

#define fastio ios_base:: sync_with_stdio(false); cin.tie(0); cout.tie(0)

#define bug(x) printf("BUG %lld\n", x)
#define pb push_back

#define mxlld LLONG_MAX
#define mnlld -LLONG_MAX

using namespace std;

bool check(ll n, ll pos)
{
	return n & (1LL << pos);
}

ll Set(ll n, ll pos)
{
	return n = n | (1LL << pos);
}

int main()
{
    ll i, j, k, l, m, n, r, q;
    ll testcase;
    ll in, ans;

    #ifdef Has9

    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);

    #endif

    while(cin >> n)
    {
        vector < ll > v(n);

        ans = 0;

        for(auto &it: v) cin >> it;

        sort(v.begin(), v.end());

        vector < ll > vor(n);

        vor.back() = v.back();

        for(ll i = n - 2; i >= 0; i--)
        {
            vor[i] = vor[i + 1] | v[i];
        }

        ans = 0;
        ll now = 0;

        for(ll i = 0; i < n; i++)
        {
            if(v[i] > now)
            {
                for(ll j = 40; j >= 0; j--)
                {
                    if(check(vor[i], j) && check(now, j) == 0)
                    {
                        ans = Set(ans, j);
                        now = Set(now, j);
                        
                        if(v[i] <= now) break;
                    }
                }
            }

            now |= v[i];
        }

        cout << ans << "\n";
    }
}
