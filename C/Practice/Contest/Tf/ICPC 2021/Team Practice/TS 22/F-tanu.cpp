#include<bits/stdc++.h>
using namespace std;

#define ll long long
#define mp make_pair
#define pb push_back
#define sc second
#define fr first
#define scl(n) scanf("%lld",&n)
#define scll(n,m) scanf("%lld%lld",&n,&m)
#define scs(ch) scanf("%s", ch)
#define pll pair< ll,ll >


ll key[200005],actv[200005],val[200005],ans=0,par_count[200005];


vector < ll > key_map[200005],chld[200005];


void op(ll a)
{

    for(ll i=0 ; i<chld[a].size(); i++)
    {
        ll x = chld[a][i];


        par_count[x]--;
        assert(par_count[x] >= 0);

        if(key[x]==0 && par_count[x]==0){
            op(x);
        }

    }
    chld[a].clear();
    ans+=val[a];
    val[a] = 0;
    return;
}



int main()
{
  
    #ifdef Has9

        freopen("input.txt", "r", stdin);
        freopen("output.txt", "w", stdout);

    #endif
    
    ll test,t,i,j,k,a,b,c,x,y,z,n,m,p,q,d;

    scll(n,m);
    scll(d,p);
    scl(q);
    for(i=1;i<=m;i++){
        scll(a,b);
        chld[b].pb(a);
        par_count[a]++;
    }
    for(i=1;i<=n;i++){
        scl(a);
        if(a==-1)continue;
        key_map[a].pb(i);
        key[i]++;
    }

    for(i=1;i<=d;i++){
        scll(a,b);
        val[a]+=b;
    }

    for(i=1;i<=n;i++){
        if(key[i]==0 && par_count[i]==0)
        {
            op(i);

        }

    }

    for(i=1;i<=q;i++){
        scl(a);

        for(j=0;j<key_map[a].size();j++){
            b=key_map[a][j];
            //cout<<a<< "   " <<b<<endl;
            key[b]--;
            assert(key[b] >= 0);
            if(key[b]==0)
            {
                if(par_count[b]==0){
                    //cout<< "  " << b<<endl;
                    op(b);
                }
            }
        }
        key_map[a].clear();

        printf("%lld\n",ans);
    }


    return 0;
}