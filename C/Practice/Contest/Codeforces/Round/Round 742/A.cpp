#include<bits/stdc++.h>

#define ll long long

using namespace std;

int main()
{
    ll i, j, k, l, m, n, r, q;
    ll testcase;
    ll in, ans;

    #ifdef Has9
        freopen("input.txt", "r", stdin);
        freopen("output.txt", "w", stdout);
    #endif 

    cin >> testcase;

    while(testcase--) {
        string str;
        cin >> str;

        string ans = "";

        for(auto it: str) {
            if(it == 'L') ans += "L";
            else if(it == 'R') ans += "R";
            else if(it == 'U') ans += "D";
            else if(it == 'D') ans += "U";
        }

        cout << ans << "\n";
    }
}