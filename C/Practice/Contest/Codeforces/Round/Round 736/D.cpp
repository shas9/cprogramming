 // Lights will guide you home

#include <bits/stdc++.h>

#define slld(longvalue) scanf("%lld", &longvalue)

#define ll long long
#define pll pair < ll, ll >

#define fastio ios_base:: sync_with_stdio(false); cin.tie(0); cout.tie(0)

#define bug(x) printf("BUG %lld\n", x)
#define pb push_back

#define mxlld LLONG_MAX
#define mnlld -LLONG_MAX

using namespace std;

bool check(ll n, ll pos)
{
	return n & (1LL << pos);
}

ll Set(ll n, ll pos)
{
	return n = n | (1LL << pos);
}

ll __gcd(ll x, ll y)
{
    if(x == 0) return y;
    if(y == 0) return x;

    return __gcd(y % x, x);
}
const ll mx = 200005;

ll arr[mx];
ll tree[mx*4];

void init(ll node, ll b, ll e)
{
    if(b == e)
    {
        if(b == 1) tree[node] = 0;
        else tree[node] = abs(arr[b] - arr[b - 1]);
        return;
    }

    ll left = node * 2;
    ll right = (node * 2) + 1;
    ll mid = (b + e) / 2;

    init(left, b, mid);
    init(right, mid + 1, e);

    tree[node] = __gcd(tree[left],tree[right]);

}


ll solve(ll node, ll b, ll e, ll i, ll j)
{
    if(i > e || j < b)
    {
        return 0;
    }

    if(b >= i && e <= j)
    {
        return tree[node];
    }

    ll left = node * 2;
    ll right = (node * 2) + 1;
    ll mid = (b + e) / 2;

    ll p1 = solve(left, b, mid, i, j);
    ll p2 = solve(right, mid + 1, e, i, j);

    return __gcd(p1,p2);
}

int main()
{
    ll i, j, k, l, m, n, r, q;
    ll testcase;
    ll in, ans;


    #ifdef Has9

        freopen("input.txt", "r", stdin);
        freopen("output.txt", "w", stdout);

    #endif

    cin >> testcase;
    
    for(ll cs = 1; cs <= testcase; cs++)
    {
        cin >> n;

        for(ll i = 1; i <= n; i++) cin >> arr[i];

        init(1,1,n);

        ll ptr1 = 2;
        ll ptr2 = 2;

        ans = 1;

        while(ptr1 <= n)
        {
            while(ptr2 < ptr1) ptr2++;
            ll g = solve(1,1,n,ptr1, ptr2);

            if(g == 1)
            {
                ptr1++;
            }
            else
            {
                ans = max(ans, ptr2 - ptr1 + 2);
                if(ptr2 + 1 <= n) ptr2++;
                else break;
            }
        }

        cout << ans << "\n";
        
    }
}
