 // Lights will guide you home

#include <bits/stdc++.h>

#define slld(longvalue) scanf("%lld", &longvalue)

#define ll long long
#define pll pair < ll, ll >

#define fastio ios_base:: sync_with_stdio(false); cin.tie(0); cout.tie(0)

#define bug(x) printf("BUG %lld\n", x)
#define pb push_back

#define mxlld LLONG_MAX
#define mnlld -LLONG_MAX

using namespace std;

bool check(ll n, ll pos)
{
	return n & (1LL << pos);
}

ll Set(ll n, ll pos)
{
	return n = n | (1LL << pos);
}


const ll mx = 200005;

ll arr[mx];
ll tree[mx*4];

void init(ll node, ll b, ll e)
{
    if(b == e)
    {
        if(b == 1) tree[node] = 0;
        else tree[node] = abs(arr[b] - arr[b - 1]);
        return;
    }

    ll left = node * 2;
    ll right = (node * 2) + 1;
    ll mid = (b + e) / 2;

    init(left, b, mid);
    init(right, mid + 1, e);

    tree[node] = __gcd(tree[left],tree[right]);

}


ll solve(ll node, ll b, ll e, ll i, ll j)
{
    if(i > e || j < b)
    {
        return 0;
    }

    if(b >= i && e <= j)
    {
        return tree[node];
    }

    ll left = node * 2;
    ll right = (node * 2) + 1;
    ll mid = (b + e) / 2;

    ll p1 = solve(left, b, mid, i, j);
    ll p2 = solve(right, mid + 1, e, i, j);

    return __gcd(p1,p2);
}

set < ll > g[200005];

ll kill(ll n)
{
    ll now = solve(1,1,n);
    ll ret = 0;

    while(now != -1)
    {
        ret++;

        for(auto it: g[now])
        {
            g[it].erase(now);
            update(1,1,n,it,-1);
            update(1,1,n,now,-1);
        }

        g[now].clear();

        now = solve(1,1,n);
    }

    return ret;
}

ll brkk[200005];

int main()
{
    ll i, j, k, l, m, n, r, q;
    ll testcase;
    ll in, ans;

    #ifdef Has9

        freopen("input.txt", "r", stdin);
        freopen("output.txt", "w", stdout);

    #endif

    while(cin >> n >> m)
    {
        for(ll i = 1; i <= n; i++) g[i].clear();

        memset(brkk,0,sizeof brkk);

        ll now = 0;

        for(ll i = 1; i <= m; i++)
        {
            ll u, v;
            cin >> u >> v;

            g[u].insert(v);
            g[v].insert(u);

            if(u < v)
            {
                brkk[u]++;

                if(brkk[u] == 1) now++; 
            }
            else
            {
                brkk[v]++;

                if(brkk[v] == 1) now++;
            }
        }

        cin >> q;

        ans = n;

        for(ll i = 1; i <= q; i++)
        {
            ll t;

            cin >> t;

            if(t == 3)
            {
                cout << n - now << "\n";
            }
            else
            {
                ll u, v;

                cin >> u >> v;

                if(t == 2)
                {
                    g[u].erase(v);
                    g[v].erase(u);

                    if(u < v)
                    {
                        brkk[u]--;

                        if(brkk[u] == 0) now--;
                    }
                    else
                    {
                        brkk[v]--;

                        if(brkk[v] == 0) now--;
                    }
                }
                else
                {
                    g[u].insert(v);
                    g[v].insert(u);

                    if(u < v)
                    {
                        brkk[u]++;

                        if(brkk[u] == 1) now++; 
                    }
                    else
                    {
                        brkk[v]++;

                        if(brkk[v] == 1) now++;
                    }
                } 
            }


        }
    }
}
