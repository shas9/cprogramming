 // Lights will guide you home

#include <bits/stdc++.h>

#define slld(longvalue) scanf("%lld", &longvalue)

#define ll long long
#define pll pair < ll, ll >

#define fastio ios_base:: sync_with_stdio(false); cin.tie(0); cout.tie(0)

#define bug(x) printf("BUG %lld\n", x)
#define pb push_back

#define mxlld LLONG_MAX
#define mnlld -LLONG_MAX

using namespace std;

bool check(ll n, ll pos)
{
	return n & (1LL << pos);
}

ll Set(ll n, ll pos)
{
	return n = n | (1LL << pos);
}

int main()
{
    ll i, j, k, l, m, n, r, q;
    ll testcase;
    ll in, ans;

    #ifdef Has9

        freopen("input.txt", "r", stdin);
        freopen("output.txt", "w", stdout);

    #endif

    cin >> testcase;
    
    for(ll cs = 1; cs <= testcase; cs++)
    {
        cin >> n;
        string s1, s2;
        cin >> s1 >> s2;

        ll took = 0;

        for(ll i = 0; i < n; i++)
        {
            if(s2[i] == '0') continue;

            if(s1[i] == '0')
            {
                took++;
                continue; 
            }

            if(i && s1[i - 1] == '1')
            {
                s1[i - 1] = '0';
                took++;
            }
            else if(i + 1 < n && s1[i + 1] == '1')
            {
                s1[i + 1] = '0';
                took++;
            }
        }

        cout << took << "\n";

    }
}
