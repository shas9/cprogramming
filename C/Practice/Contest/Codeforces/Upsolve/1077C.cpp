 // Lights will guide you home

#include <bits/stdc++.h>

#define slld(longvalue) scanf("%lld", &longvalue)

#define ll long long
#define pll pair < ll, ll >

#define fastio ios_base:: sync_with_stdio(false); cin.tie(0); cout.tie(0)

#define bug(x) printf("BUG %lld\n", x)
#define pb push_back

#define mxlld LLONG_MAX
#define mnlld -LLONG_MAX

using namespace std;

bool check(ll n, ll pos)
{
	return n & (1LL << pos);
}

ll Set(ll n, ll pos)
{
	return n = n | (1LL << pos);
}

map < ll, vector < ll > > g;

int main()
{
    ll i, j, k, l, m, n, r, q;
    ll testcase;
    ll in, ans;

    #ifdef Has9

        freopen("input.txt", "r", stdin);
        freopen("output.txt", "w", stdout);

    #endif

    while(cin >> n)
    {
        g.clear();

        vector < ll > v(n + 1);

        ll tot = 0;

        for(ll i = 1; i <= n; i++) 
        {
            cin >> v[i];
            g[v[i]].push_back(i);
            tot += v[i];
        }

        set < ll > ans;

        for(ll i = 1; i <= n; i++)
        {
            ll need = tot - v[i];

            if(need < v[i]) continue;

            ll fnd = (need - v[i]);

            for(auto it: g[fnd])
            {
                if(it != i) ans.insert(it);

                //cout << i << ": " << v[i] << " " << need << " " << fnd << " " << it << endl;
            }
        }
        
        cout << ans.size() << "\n";

        for(auto it: ans) cout << it << " ";

        cout << "\n";
    }
}