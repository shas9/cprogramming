#include <stdio.h>
#include <math.h>

int main()
{
    int arr[18000];
    int prime[2000];

    int i, j, sq;

    sq = sqrt(18000);


    for(i = 0; i < 18000; i++)
    {
        arr[i] = 1;
    }

    for(i = 0; i < 2000; i++)
    {
        prime[i] = 0;
    }


    for(i = 2; i <= 18000; i++)
    {
        if(arr[i] == 1)
        {
            for(j = i + i; j <= 18000; j = j + i)
            {
                arr[j] = 0;
            }
        }
    }

    for(i = 2, j = 0; i < 18000; i++)
    {
        if(arr[i] == 1)
        {
            prime[j] = i;

            j++;
        }
    }

    for(i = 0; i < 2000; i++)
    {
        printf("%d  ", prime[i]);
    }

    return 0;
}
