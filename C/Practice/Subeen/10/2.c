#include <stdio.h>

int main()
{
    char saarc[7][20];

    int i;

    for(i = 0; i < 7; i++)
    {
        scanf("%s", saarc[i]);
    }

    int j, ln;

    for(i = 0; i < 7; i++)
    {
        for(ln = 0; saarc[i][ln] != '\0'; ln++);

        printf("\n");

        for(j = 0; j < ln; j++)
        {
            printf("row = %d, col = %d == %c \nb", i, j, saarc[i][j]);
        }
    }

    return 0;
}
