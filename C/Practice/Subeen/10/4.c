#include<stdio.h>

int main()
{
    int x[5][5] = {6, 4, 7, 8, 9, 3, 7, 1, 9, 9, 8, 6, 4, 2, 7, 2, 4, 2, 5, 9, 4, 1, 6, 7, 3};

    int i, j;

    for(i = 0; i < 5; i++)
    {
        for(j = 0; j < 5; j++)
        {
            printf("%d ", x[i][j]);
        }
        printf("\n");
    }

    for(i = 0; i < 5; i++)
    {
        for(j = 1; j < 5; j++)
        {
            x[i][0] = x[i][0] + x[i][j];
        }
    }

    for(i = 0; i < 5; i++)
    {
        printf("The sum of %d th row is = %d\n",i + 1, x[i][0]);
    }

    return 0;
}
