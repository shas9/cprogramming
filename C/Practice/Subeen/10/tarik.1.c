#include <stdio.h>

int main()
{
    int x[] = {2, 9, 6, 7, 8, 5, 4, 1, 67, 54};

    int i, j;

    int t;

    for(i = 0; i < 9; i++)
    {
        for(j = 0; j < 9; j++)
        {
            if(x[j] > x[j + 1])
            {
               t = x[j];
               x[j] = x[j + 1];
               x[j + 1] = t;
            }
        }
    }

    for(i = 0; i < 10; i++)
    {
        printf("%d ", x[i]);
    }

    return 0;

}
