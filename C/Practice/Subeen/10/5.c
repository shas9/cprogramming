#include <stdio.h>

int main()
{
    int x[5][5] = {6, 4, 7, 8, 9, 3, 7, 1, 9, 9, 8, 6, 4, 2, 7, 2, 4, 2, 5, 9, 4, 1, 6, 7, 3};

    int i, j;

    printf("The main matrix is: \n");

    for(i = 0; i < 5; i++)
    {
        for(j = 0; j < 5; j++)
        {
            printf("%d ", x[i][j]);
        }
        printf("\n");
    }

    int y[5][5];

    for(i = 0; i < 5; i++)
    {
        for(j = 0; j < 5; j++)
        {
            y[i][j] = x[j][i];
        }

    }

    printf("The Inverse matrix is: \n");

    for(i = 0; i < 5; i++)
    {
        for(j = 0; j < 5; j++)
        {
            printf("%d ", y[i][j]);
        }
        printf("\n");
    }


}
