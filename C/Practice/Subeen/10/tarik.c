#include <stdio.h>

int main()
{
    int a[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

    int i;

    for(i = 10; i > 2; i--)
    {
        a[i] = a[i - 1];
    }

    a[3] = 5;

    for(i = 0; i < 11; i++)
    {
        printf("%d ", a[i]);
    }

    return 0;
}
