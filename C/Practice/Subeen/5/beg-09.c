#include <stdio.h>

int main()
{
    char name;
    double salary, productsold, finalsalary;

    scanf("%s%lf%lf", &name, &salary, &productsold);

    finalsalary = salary + (productsold * 0.15);

    printf("TOTAL = R$ %.2lf\n", finalsalary);

    return 0;
}
