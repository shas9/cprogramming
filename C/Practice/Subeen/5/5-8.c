#include <stdio.h>

int main()
{
    double x, y;

    scanf("%lf", &x);

    y = (x - 32) / 1.8;

    printf("The Celsius is: %.2lf", y);

    return 0;
}
