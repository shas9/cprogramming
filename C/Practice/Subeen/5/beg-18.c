#include<stdio.h>

int main()
{
    int amount;
    int t,u,v,w,x,y,z;
    int a,b,c,d,e,f;

    scanf("%d", &amount);

    t = amount / 100;
    a = amount % 100;

    u = a / 50;
    b = a % 50;

    v = b / 20;
    c = b % 20;

    w = c / 10;
    d = c % 10;

    x = d / 5;
    e = d % 5;

    y = e / 2;
    f = e % 2;

    z = f / 1;

    printf("%d\n%d nota(s) de R$ 100,00\n%d nota(s) de R$ 50,00\n%d nota(s) de R$ 20,00\n%d nota(s) de R$ 10,00\n%d nota(s) de R$ 5,00\n%d nota(s) de R$ 2,00\n%d nota(s) de R$ 1,00\n", amount, t, u, v, w, x, y, z);

    return 0;

}
