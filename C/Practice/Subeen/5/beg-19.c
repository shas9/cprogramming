#include <stdio.h>

int main()
{
    int time, second, min, hour;
    int a, b;

    scanf("%d", &time);

    hour = time / 3600;
    a = time % 3600;

    min = a / 60;
    b = a % 60;

    second = b;

    printf("%d:%d:%d\n", hour, min, second);

    return 0;


}
