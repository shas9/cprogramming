#include <stdio.h>

int main()
{
    double a, b, c;
    double triangle, circle, trapezium, square, rectangle;
    double pi;

    pi = 3.14159;

    scanf("%lf%lf%lf", &a, &b, &c);

    triangle = (a * c) / 2;
    circle = pi * c * c;
    trapezium = ((a+b) / 2) * c;
    square = b * b;
    rectangle = a * b;

    printf("TRIANGULO: %.3lf\nCIRCULO: %.3lf\nTRAPEZIO: %.3lf\nQUADRADO: %.3lf\nRETANGULO: %.3lf\n", triangle, circle, trapezium, square, rectangle);

    return 0;
}
