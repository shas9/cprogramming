#include <stdio.h>

int main()
{
    int l, y;
    double i, x, z;

    printf("Type the amount of loan:");
    scanf("%d", &l);

    printf("\nType the amount of interest:");
    scanf("%lf", &i);

    printf("\nType Year:");
    scanf("%d", &y);

    x = (l * i/100);
    z = (x + l)/12;

    printf("\nYour amount is %.2lf\n", z);

    return 0;

}
