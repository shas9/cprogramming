#include <stdio.h>

int main()
{
    double a, u, t, s;

    printf("a =");
    scanf("%lf", &a);

    printf("\nu =");
    scanf("%lf", &u);

    printf("\nt=");
    scanf("%lf", &t);

    s = (u * 2 * t) + (2 * a * t * t);

    printf("Distance is %.0lf m", s);

    return 0;
}
