#include <stdio.h>

int main()
{
    int speed, time;

    scanf("%d%d", &time, &speed);

    double fuel, x, y;

    x = time;
    y = speed;

    fuel = ((y * x) / 12);

    printf("%.3lf\n", fuel);

    return 0;
}
