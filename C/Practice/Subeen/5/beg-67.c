#include <stdio.h>

int main()
{
    int x, n;
    scanf("%d", &n);

    for(x = 1; x <= n; x = x + 2)
    {
        printf("%d\n", x);
    }
    return 0;
}
