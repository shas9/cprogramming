#include<stdio.h>

int main()
{
    double amount;
    double t,u,v,w,x,y;
    double a,b,c,d,e,f;

    scanf("%lf", &amount);

    t = amount / 100.0;
    a = amount % 100.0;

    u = a / 50;
    b = a % 50;

    v = b / 20;
    c = b % 20;

    w = c / 10;
    d = c % 10;

    x = d / 5;
    e = d % 5;

    y = e / 2;
    f = e % 2;

    double g,h,i,j,k;
    double m,n,o,p,q,z;

    z = f / 1;
    g = f % 1;

    m = g / 0.5;
    h = g % 0.5;

    n = h / 0.25;
    i = h % 0.25;

    o = i / 0.10;
    j = i % 0.10;

    p = j / 0.05;
    k = j % 0.05;

    q = j / 0.01;

    printf("NOTAS:\n%.0lf nota(s) de R$ 100,00\n%.0lf nota(s) de R$ 50,00\n%.0lf nota(s) de R$ 20,00\n%.0lf nota(s) de R$ 10,00\n%.0lf nota(s) de R$ 5,00\n%.0lf nota(s) de R$ 2,00\n" t, u, v, w, x, y);

    printf("MOEDAS:\n%.0lf moeda(s) de R$ 1.00\n%.0lf moeda(s) de R$ 0.50\n%.0lf moeda(s) de R$ 0.25\n%.0lf moeda(s) de R$ 0.10\n%.0lf moeda(s) de R$ 0.05\n%.0lf moeda(s) de R$ 0.01\n", z, m, n, o, p, q);

    return 0;

}

