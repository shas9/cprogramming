#include<stdio.h>

int main()
{
    int code, quantity;

    scanf("%d%d", &code, &quantity);

    double amount;

    if(code == 1)
    {
        amount = 4;
    }
    else if(code == 2)
    {
        amount = 4.5;
    }
    else if(code == 3)
    {
        amount = 5;
    }
    else if(code == 4)
    {
        amount = 2;
    }
    else if(code == 5)
    {
        amount = 1.5;
    }
    double value;

    value = amount * quantity;

    printf("Total: R$ %.2lf\n", value);

    return 0;
}
