#include <stdio.h>

int main()
{
    int a, b, c, d;

    a = 89;
    b = 77;
    c = 65;

    d = (a / 4) + (b / 4) + (c / 2);

    printf("Your result is: %d", d);

    return 0;
}
