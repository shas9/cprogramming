#include <stdio.h>

int main()
{

    double a1, a2, b1, b2, c1, c2, x, y, d;

    printf("Type a1:");
    scanf("%lf", &a1);

    printf("Type b1:");
    scanf("%lf", &b1);

    printf("Type a2:");
    scanf("%lf", &a2);

    printf("Type b2:");
    scanf("%lf", &b2);

    printf("Type c1:");
    scanf("%lf", &c1);

    printf("Type c2:");
    scanf("%lf", &c2);

    printf("\nSo your equation is %.0lfx + %.0lfy = %.0lf\n", a1, b1, c1);
    printf("And %.0lfx + %.0lfy = %.0lf\n", a2, b2, c2);

    d = (a1*b2 - a2*b1);

    x = (b1*c2 - b2*c1) / d;
    y = (a1*c2 - a2*c1) / d;

    printf("\nThe value of x is %.3lf.\nAnd the value of Y is %.3lf\n", x, y);

    return 0;


}
