#include <stdio.h>

int main()
{
    int a, b, c;

    c = 0;

    printf("Enter the limit:");
    scanf("%d", &b);

    for(a = 1; a <= b; a = a + 2)
    {
       c = c + a;
    }
    printf("The result is %d", c);

    return 0;
}
