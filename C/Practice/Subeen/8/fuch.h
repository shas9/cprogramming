#include"fuch.h"

int findpos(int arr[], int size, int num)
{
    int high, low, mid;

    high = size - 1;
    low = 0;

    while(low <= high)
    {
        mid = (low + high) / 2;

        if(num == arr[mid])
        {
            return (mid + 1);
            break;
        }
        if(num < arr[mid])
        {
            high = mid - 1;
        }
        else
        {
            low = mid + 1;
        }
    }

    if(low > high)
    {
        return 0;
    }
}
