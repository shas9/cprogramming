#include <stdio.h>

int main()
{
    int arr[16];

    int i;

    for(i = 0; i < 16; i++)
    {
        scanf("%d", &arr[i]);
    }

    int low = 0;
    int high = 15;

    int mid;

    int num;

    printf("TYPE YOUR NUMER: ");

    scanf("%d", &num);

    while(low <= high)
    {
        mid = (low + high) / 2;

        if(num == arr[mid])
        {
            break;
        }
        if(num < arr[mid])
        {
            high = mid - 1;
        }
        else
        {
            low = mid + 1;
        }
    }

    if(low > high)
    {
        printf("\n%d is not in the array\n", num);
    }
    else
    {
        printf("\n%d is found in the array in %dth element", arr[mid], mid + 1);
    }

    return 0;

}
