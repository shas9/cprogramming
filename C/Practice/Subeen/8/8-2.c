#include<stdio.h>
#include "fuch.h"

int main()
{
    int size;

    scanf("%d", &size);

    int arr[size];

    int i;
    for(i = 0; i < size; i++)
    {
        scanf("%d", &arr[i]);
    }

    int num;

    scanf("%d", &num);

    int pos;

    pos = findpos(arr, size, num);

    printf("%d is in the %dth position\n", num, pos);

    return 0;
}
