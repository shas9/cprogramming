#include <stdio.h>

int main()
{
    int arr[10];

    int i;

    for(i = 0; i < 10; i++)
    {
        scanf("%d", &arr[i]);
    }

    int max1;

    max1 = min(arr, 10);

    printf("%d is the min number\n", max1);

    return 0;
}

int min(int arr[], int n)
{
    int i;

    int min = arr[0];

    for(i = 1; i < n; i++)
    {
        if(arr[i] < min)
        {
            min = arr[i];
        }

    }

    return min;
}
