#include <stdio.h>

int add(int x, int y);

int main()
{
    int a, b, c;

    scanf("%d%d", &a, &b);

    c = add(a,b);

    printf("SUM = %d\n", c);

    return 0;
}

int add(int x, int y)
{
    int z = x + y;

    return z;
}
