#include <stdio.h>

int sum(int arr[], int n)
{
    int i;

    int sum = 0;

    for(i = 0; i < n; i++)
    {
        sum = sum + arr[i];
    }

    return sum;
}

int main()
{
    int arr[5];

    int i;

    for(i = 0; i < 5; i++)
    {
        scanf("%d", &arr[i]);
    }

    int x;

    x = sum(arr, 5);

    printf("\n\n%d\n", x);

    return 0;
}
