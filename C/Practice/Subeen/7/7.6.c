#include <stdio.h>

int main()
{
    int arr[10];

    int i;

    for(i = 0; i < 10; i++)
    {
        scanf("%d", &arr[i]);
    }

    int max1;

    max1 = max(arr, 10);

    printf("%d is the max number\n", max1);

    return 0;
}

int max(int arr[], int n)
{
    int i;

    int max = arr[0];

    for(i = 1; i < n; i++)
    {
        if(arr[i] > max)
        {
            max = arr[i];
        }

    }

    return max;
}
