#include <stdio.h>

int pow(int num, int p)
{
    int i;

    int x = num;

    for(i = 2; i <= p; i++)
    {
        x = num * x;
    }

    return x;
}

int main()
{
    int num, p;

    scanf("%d%d", &num, &p);

    int x;

    x = pow(num, p);

    printf("%d\n", x);

    return 0;
}
