#include <stdio.h>

int test_condition(int x)
{
    int y = x;
    x = 2 * y;

    return (x * y);
}

int main()
{
    int x, z, y;

    scanf("%d%d", &x, &z);

    y = test_condition(z);

    printf("Res = %d\n", y);

    return 0;
}
