#include <stdio.h>

int main()
{
    int x;
    printf("Enter your number:\n");
    scanf("%d", &x);

    int y;
    y = x % 2;

    if (y == 0)
    {
        printf("The number is even.\n");
    }
    else
    {
        printf("The number is odd.\n");
    }
    return 0;
}
