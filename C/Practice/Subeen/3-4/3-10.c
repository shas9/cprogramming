#include <stdio.h>

int main()
{
    char x;
    printf("Enter your word is:\n");
    scanf("%c", &x);

    if(x >= 'A' && x <= 'Z')
    {
        printf("Your word %c is Uppercase\n", x);
    }
    else if(x >= 'a' && x <= 'z')
    {
        printf("Your word %c is lowercase\n", x);
    }
    else
    {
        printf("This is syntax = %c.\n", x);
    }
    return 0;
}
