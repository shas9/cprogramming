#include <stdio.h>

int main()
{
    int x, y;

    printf("Enter the starting:\n");
    scanf("%d", &x);

    printf("Enter the Ending:\n");
    scanf("%d", &y);

    printf("The Numbers are:\n");

    while (x < y)
    {
        x = x + 1;
        if (x % 2 == 0)
        {
            continue;
        }
        printf("%d\n", x);
    }
    return 0;
}
