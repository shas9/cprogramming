#include<stdio.h>

int main()
{
    int x;
    printf("Enter your number:\n");
    scanf("%d", &x);

    if(x <= 1 || x >= 10)
    {
        printf("The number is valid.\n");
    }
    else
    {
        printf("The number is invalid.\n");
    }
    return 0;
}
