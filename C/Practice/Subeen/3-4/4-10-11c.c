#include <stdio.h>

int main()
{
    int a, b, c;
    c = 0;

    for(a = 2; a <= 20; a++)
    {
        for(b = 1; b <= 10; b++)
        {
            c = c + a;
            printf("%d X %d = %d\n", a, b, a * b);
        }
    }
    return 0;
}
