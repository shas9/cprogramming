#include <stdio.h>

int main()
{
    int a, b;
    a = 16;
    b = 1;

    for( ; ; )
    {
        printf("%d X %d = %d\n", a, b, a * b);
        b = b + 1;
        if(b > 30)
        {
            break;
        }
    }
    return 0;
}
