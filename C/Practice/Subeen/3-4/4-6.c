#include<stdio.h>

int main()
{
    int x, s, e;

    printf("Enter the Number:\n");
    scanf("%d", &x);

    printf("Enter the starting:\n");
    scanf("%d", &s);

    printf("Enter the ending:\n");
    scanf("%d", &e);

    printf("The Numbers are:\n");

    while (s <= e)
    {
        printf("%d x %d = %d\n", x, s, x*s);
        s = s + 1;
    }
    return 0;
}

