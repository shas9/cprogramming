#include <stdio.h>

int main()
{
    int a, b, c;
    c = 0;

    for(a = 2; a <= 10; a++)
    {
        for(b = 1; b <= 10; b++)
        {
            printf("%d X %d = %d\n", a, b, a + c);
            c = a + c;
            if(b == 10)
            {
                continue;
            }

        }
    }
    return 0;
}
