#include <stdio.h>

int main()
{
    int x, y;

    printf("Enter the starting point:\n");
    scanf("%d", &x);

    printf("Enter the ending point:\n");
    scanf("%d", &y);

    while(x <= y)
    {
        x = x + 1;
        if(x % 2 == 0)
        {
            continue;
        }
        printf("%d\n", x);
    }
    return 0;
}
