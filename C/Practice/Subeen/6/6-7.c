#include<stdio.h>

int main()
{
    int ft[40], st[40], at[40];

    int i, j;

    for(i = 0, j = 45; i < 40 && j <= 100; i++, j++)
    {
        ft[i] = j;
        st[i] = (100 - j);
        at[i] = (20 + j);
    }

    double total;

    for(i = 0; i < 40; i++)
    {
        total = (ft[i] / 4.0) + (st[i] / 4.0) + (at[i] / 2.0);

        printf("** Roll no: %d\tTotal marks: %.2lf\n\n\n", i + 1, total);

    }

    return 0;


}
