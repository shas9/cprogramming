#include <stdio.h>

int main()
{
    int testcase, input;

    scanf("%d", &testcase);

    int in = 0, out = 0;

    int i;

    for(i = 1; i <= testcase; i++)
    {
        scanf("%d", &input);

        if(input >= 10 && input <= 20)
        {
            in = in + 1;
        }
        else
        {
            out = out + 1;
        }
    }
    printf("%d in\n%d out\n", in, out);
    return 0;
}
