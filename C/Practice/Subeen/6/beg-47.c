#include <stdio.h>

int main()
{
    int inh, inm, fnh, fnm;
    int in, fn;

    scanf("%d%d%d%d", &inh, &inm, &fnh, &fnm);

    in = (inh * 60) + inm;
    fn = (fnh * 60) + fnm;

    int result, hour, min;

    if(fn <= in)
    {
        fn = fn + 1440;
    }

    result = fn - in;

    hour = result / 60;
    min = result % 60;

    printf("O JOGO DUROU %d HORA(S) E %d MINUTO(S)\n", hour, min);

    return 0;
}
