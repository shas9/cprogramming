#include <stdio.h>

int main()
{
    int sday, sh, sm, ss, st;
    int eday, eh, em, es, et;

    char c[4], b[4];

    scanf("%s", c);
    scanf("%d", &sday);

    scanf("%d : %d : %d", &sh, &sm, &ss);
    // printf("%d : %d : %d\n",  sh, sm, ss);

    scanf("%s", b);
    scanf("%d", &eday);

    scanf("%d : %d : %d", &eh, &em, &es);
    // printf("%d : %d : %d\n", eh, em, es);



    st = (sh * 3600) + (sm * 60) + ss;
    et = (eh * 3600) + (em * 60) + es;

    if (et < st)
    {
        eday = eday - 1;
        et = et + 86400;
    }

    int rday, rh, rm, rs, rt;

    int a;

    rday = eday - sday;
    rt = et - st;

    rh = rt / 3600;
    a = rt % 3600;

    rm = a / 60;

    rs = a % 60;

    printf("%d dia(s)\n%d hora(s)\n%d minuto(s)\n%d segundo(s)\n", rday, rh, rm, rs);

    return 0;
}
