#include <stdio.h>

int main()
{
    int testcase, ch, i;
    int input;
    int rabbit = 0, rat = 0, frog = 0, total;
    char animal;

    scanf("%d", &testcase);

    for(i = 1; i <= (testcase * 2); i++)
    {
        scanf("%d", &input);
        scanf("%c", &animal);

        if(animal == 'C' || animal == 'c')
        {
            rabbit = rabbit + input;
        }
        else if(animal == 'R' || animal == 'r')
        {
            rat = rat + input;
        }
        else if(animal == 'S' || animal == 's')
        {
            frog = frog + input;
        }
    }

    total = rabbit + rat + frog;

    double pr1, pr2, pf;

    pr1 = (rabbit * 100.0) / total;
    pr2 = (rat * 100.0) / total;
    pf = (frog * 100.0) / total;

    //char x = '%';

    printf("Total: %d cobaias\nTotal de coelhos: %d\nTotal de ratos: %d\nTotal de sapos: %d\nPercentual de coelhos: %.2lf %\nPercentual de ratos: %.2lf %\nPercentual de sapos: %.2lf %\n", total, rabbit, rat, frog, pr1, pr2, pf);
    return 0;
}
