#include <stdio.h>

int main()
{
    int number;
    int i, result;

    scanf("%d", &number);

    for(i = 1; i <= 10; i++)
    {
        result = i * number;

        printf("%d x %d = %d\n", i, number, result);
    }

    return 0;
}
