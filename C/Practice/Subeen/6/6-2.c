#include <stdio.h>

int main()
{
    int x[5] = {1, 2, 3, 4, 5};

    printf("1 = %d\n", x[0]);
    printf("4 = %d\n", x[3]);

    return 0;
}
