#include <stdio.h>

int main()
{
    double salary, per, increament, amount;

    scanf("%lf", &salary);

    if(0 <= salary && salary <= 400)
    {
        per = 15;
    }
    else if(400 < salary && salary <= 800)
    {
        per = 12;
    }
    else if(800 < salary && salary <= 1200)
    {
        per = 10;
    }
    else if(1200 < salary && salary <= 2000)
    {
        per = 7;
    }
    else if(2000 <= salary)
    {
        per = 4;
    }

    increament = salary * (per/100);

    amount = salary + increament;

    printf("Novo salario: %.2lf\nReajuste ganho: %.2lf\nEm percentual: %.0lf %\n", amount, increament, per);

    return 0;
}


