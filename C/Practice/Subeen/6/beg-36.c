#include <stdio.h>

int main()
{
    double a, b, c;

    double root;

    double r1, r2, squ;

    scanf("%lf%lf%lf", &a, &b, &c);

    root = (b * b) - (4 * a * c);

    squ = sqrt(root);

    r1 = (-b + squ) / (2*a);
    r2 = (-b - squ) / (2*a);

    if (a == 0 || root < 0)
    {
        printf("Impossivel calcular\n");
    }
    else
    {
        printf("R1 = %.5lf\nR2 = %.5lf\n", r1, r2);
    }

    return 0;
}
