#include <stdio.h>

int main()
{
    int testcase;
    int i;

    scanf("%d", &testcase);

    double x, y, z, result;

    for(i = 1; i <= testcase; i++)
    {
        scanf("%lf%lf%lf", &x, &y, &z);

        result = ((x * 2) + (y * 3) + (z * 5)) / 10;

        printf("%.1lf\n", result);
    }
    return 0;
}
