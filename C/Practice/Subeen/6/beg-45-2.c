#include <stdio.h>

int main()
{
    double x, y, z;

    scanf("%lf%lf%lf", &x, &y, &z);

    double a, b, c;

    if(x >= y && x >= z)
    {
        a = x;
        b = y;
        c = z;
    }
    else if(y >= x && y >= z)
    {
        a = y;
        b = x;
        c = z;
    }
    else if(z >= y && z >= x)
    {
        a = z;
        b = x;
        c = y;
    }

    if(a >= (b + c))
    {
        printf("NAO FORMA TRIANGULO\n");
    }
    else if ( (a * a) == ((b * b) + (c * c)))
    {
        printf("TRIANGULO RETANGULO\n");
    }
    else if ((a * a) > ((b * b) + (c * c)))
    {
        printf("TRIANGULO OBTUSANGULO\n");
    }
    else if ((a * a) < ((b * b) + (c * c)))
    {
        printf("TRIANGULO ACUTANGULO\n");
    }

    if( a == b && b == c && a == c)
    {
        printf("TRIANGULO EQUILATERO\n");
    }
    else if( a == b || b == c || a == c)
    {
        printf("TRIANGULO ISOSCELES\n");
    }

    return 0;
}
