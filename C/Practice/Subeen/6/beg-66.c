#include <stdio.h>

int main()
{
    int a, b, c, d, e;

    scanf("%d%d%d%d%d", &a, &b, &c, &d, &e);

    int even = 0;

    if (a % 2 == 0)
    {
        even = even + 1;
    }

    if (b % 2 == 0)
    {
        even = even + 1;
    }

    if (c % 2 == 0)
    {
        even = even + 1;
    }

    if (d % 2 == 0)
    {
        even = even + 1;
    }

    if (e % 2 == 0)
    {
        even = even + 1;
    }

    printf("%d valor(es) par(es)\n", even);


    int odd = 0;

    if (a % 2 != 0)
    {
        odd = odd + 1;
    }

    if (b % 2 != 0)
    {
        odd = odd + 1;
    }

    if (c % 2 != 0)
    {
        odd = odd + 1;
    }

    if (d % 2 != 0)
    {
        odd = odd + 1;
    }

    if (e % 2 != 0)
    {
        odd = odd + 1;
    }

    printf("%d valor(es) impar(es)\n", odd);

    int positive = 0;

    if (a > 0)
    {
        positive = positive + 1;
    }

    if (b > 0)
    {
        positive = positive + 1;
    }


    if (c > 0)
    {
        positive = positive + 1;
    }

    if (d > 0)
    {
        positive = positive + 1;
    }

    if (e > 0)
    {
        positive = positive + 1;
    }

    printf("%d valor(es) positivo(s)\n", positive);

    int negative = 0;

    if (a < 0)
    {
        negative = negative + 1;
    }

    if (b < 0)
    {
        negative = negative + 1;
    }


    if (c < 0)
    {
        negative = negative + 1;
    }

    if (d < 0)
    {
        negative = negative + 1;
    }

    if (e < 0)
    {
        negative = negative + 1;
    }

    printf("%d valor(es) negativo(s)\n", negative);


    return 0;
}



