#include <stdio.h>

int main()
{
    int testcase, input;

    scanf("%d", &testcase);

    int i;

    for(i = 1; i <= testcase; i++)
    {
        scanf("%d", &input);

        if(input == 0)
            {
                printf("NULL\n");
            }

        else if(input % 2 != 0)
            {
                if(input < 0)
                {
                    printf("ODD NEGATIVE\n");
                }
                else
                {
                     printf("ODD POSITIVE\n");
                }
            }
        else
        {
             if(input < 0)
                {
                    printf("EVEN NEGATIVE\n");
                }
                else
                {
                     printf("EVEN POSITIVE\n");
                }
        }
    }
    return 0;

}
