#include <stdio.h>

int main()
{
    int x[10] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

    int i, j, temp;

    for(i = 0, j = 9; i <= 4 && j >= 5; i++, j--)
    {
        temp = x[j];
        x[j] = x [i];
        x[i] = temp;
    }

    for(i = 0; i <= 9; i++)
    {
        printf("%dth no is %d\n\n", i + 1, x[i]);
    }

    return 0;
}
