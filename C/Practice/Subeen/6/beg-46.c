#include <stdio.h>

int main()
{
    int start, end, time;

    scanf("%d%d", &start, &end);

    if(end <= start)
    {
        end = end + 24;
        time = end - start;
    }
    else
    {
        time = end - start;
    }

    printf("O JOGO DUROU %d HORA(S)\n", time);

    return 0;
}
