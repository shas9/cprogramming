#include <stdio.h>

int main()
{
    int x[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    int y[10];

    int i, j;

    for(i = 9, j = 0; i >= 0 && j <= 9; j++, i--)
    {
        y[j] = x [i];
    }

    for(i = 0; i <= 9; i++)
    {
        printf("y[%d] th element is %d\n", i, y[i]);
    }
    return 0;
}
