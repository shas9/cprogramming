#include <stdio.h>

int main()
{
    int x[6] = {4, 6, 7, 9, 10, 98};

    int i;

    for(i = 0; i < 6; i++)
    {
        printf("%d th element is %d\n\n", i+1, x[i]);
    }
    return 0;
}
