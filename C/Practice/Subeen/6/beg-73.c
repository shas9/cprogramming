#include <stdio.h>

int main()
{
    int input;
    int i;
    int result;

    scanf("%d", &input);

    if (input % 2 != 0)
    {
        input = input - 1;
    }

    for(i = 2; i <= input; i = i + 2)
    {
        result = i * i;
        printf("%d^2 = %d\n", i, result);
    }
    return 0;
}
