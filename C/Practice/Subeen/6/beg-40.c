#include <stdio.h>

int main()
{
    double a, b, c, d;

    double result;

    double pin, fin;

    scanf("%lf%lf%lf%lf", &a, &b, &c, &d);

    result = ((a * 2) + (b * 3) + (c * 4) + (d * 1)) / 10;

    printf("Media: %.1lf\n", result);

    if (result >= 7)
    {
        printf("Aluno aprovado.\n");
    }
    else if (result < 5)
    {
        printf("Aluno reprovado.\n");
    }
    else
    {
        printf("Aluno em exame.\n");

        scanf("%lf", &pin);

        printf("Nota do exame: %.1lf\n", pin);

        fin = (pin + result) / 2;

        if (fin >= 5)
        {
            printf("Aluno aprovado.\nMedia final: %.1lf\n", fin);

        }
        else
        {
            printf("Aluno reprovado.\nMedia final: %.1lf\n", fin);
        }
    }
    return 0;
}
