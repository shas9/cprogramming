#include <stdio.h>

int main()
{
    int input;
    int count = 0, position;

    int i;

    for(i = 1; i <= 100; i++)
    {
        scanf("%d", &input);

        if(input >= count)
        {
            count = input;
            position = i;
        }
    }

    printf("%d\n%d\n", count, position);
}
