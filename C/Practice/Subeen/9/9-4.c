#include<stdio.h>
#include<string.h>

int main()
{
    char s[1002], word[100];

    int i, j, length, isw;

    gets(s);

    length = strlen(s);

    isw = 0;

    for(i = 0, j = 0; i < length; i++)
    {
        if(s[i] >= 'a' && s[i] <= 'z')
        {
            if(isw == 0)
            {
                isw = 1;
                word[j] = 'A' + s[i] - 'a';
                j++;
            }
            else
            {
                word[j] = s[i];
                j++;
            }
        }
        else if(s[i] >= 'A' && s[i] <= 'Z')
        {
            if(isw == 0)
            {
                isw = 1;
            }
            word[j] = s[i];
            j++;
        }
        else if(s[i] >= '0' && s[i] <= '9')
        {
            if(isw == 0)
            {
                isw = 1;
            }
            word[j] = s[i];
            j++;
        }
        else
        {
            if(isw == 1)
            {
                isw = 0;
                word[j] = '\0';
                printf("%s\n", word);
                j = 0;
            }
        }
    }
    return 0;
}
