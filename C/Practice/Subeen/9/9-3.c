#include <stdio.h>

int stcom(char a[], char b[])
{
    int i, j;

    for(i = 0; a[i] != '\0' && b[i] != '\0'; i++)
    {
        if(a[i] < b[i])
        {
            return -1;
        }
        if(a[i] > b[i])
        {
            return 1;
        }
    }

    int lna, lnb;

    for(lna = 0; a[lna] != '\0'; lna++);

    for(lnb = 0; b[lnb] != '\0'; lnb++);

    if(lna == lnb)
    {
        return 0;
    }
    if(lna < lnb)
    {
        return -1;
    }
    if(lna > lnb)
    {
        return 1;
    }
}

int main()
{
    char a[200];

    char b[200];

    read:

    scanf("%s", a);
    scanf("%s", b);

    int x;

    x = stcom(a,b);

    if(x == 1)
    {
        printf("A won.\n");
    }

    else if(x == -1)
    {
        printf("B Won.\n");
    }
    else
    {
        printf("WE WON.\n");
    }
    goto read;
    return 0;
}
