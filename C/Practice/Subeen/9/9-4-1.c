#include <stdio.h>

int main()
{
    char a[200];

    scanf("%[^\n]s", &a);

    int i, ln, x;
    x = 0;

    for(ln = 0; a[ln] != '\0'; ln++);

    for(i = 0; i < ln; i++)
    {
        if(x == 0)
        {
            if(a[i] >= 'a' && a[i] <= 'z')
            {
                a[i] = a[i] - ('a' - 'A');
            }
            x = 1;
        }
        if(a[i] >= 'a' && a[i] <= 'z')
        {
            printf("%c", a[i]);
        }
        else if(a[i] >= 'A' && a[i] <= 'Z')
        {
            printf("%c", a[i]);
        }
        else if(a[i] >= '0' && a[i] <= '9')
        {
            printf("%c", a[i]);
        }
        else
        {
            x = 0;
            printf("\n");
        }

    }
    printf("\n");

    return 0;
}
