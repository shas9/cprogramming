#include<bits/stdc++.h>
#define ll long long
#define dbg(x) cout<<#x<<": "<<x<<endl;
#define N 300005
#define M 1000000007
#define pii pair<ll,ll>
#define fast ios_base::sync_with_stdio(0);cin.tie(0);
using namespace std;
 
bool has(vector<int>&v)
{
    if(v.size()<3)
        return false;
    for(int i=0;i<v.size();i++)
    {
        for(int j=0;j<v.size();j++)
        {
            for(int k=0;k<v.size();k++)
            {
                if(i!=j&&j!=k&&i!=k)
                {
                    if(abs(v[i]-v[j])+abs(i-j)==abs(v[i]-v[k])+abs(i-k)+abs(v[k]-v[j])+abs(k-j))
                    {
                        return true;
                    }
                }
            }
        }
    }
    return false;
}
void solve()
{
//    vector<int>v;
//    for(int i=1;i<=5;i++)
//        v.push_back(i);
//    bool f=0;
//    do
//    {
//        f|=!has(v);
//    }while(next_permutation(v.begin(),v.end()));
//    cout<<f<<endl;
    int n;
    cin>>n;
    int a[n];
    for(int i=0;i<n;i++) cin>>a[i];
    ll ans=0;
    for(int i=0;i<n;i++)
    {
        vector<int>v;
        v.push_back(a[i]);
        ans++;
        for(int j=i+1;j<n;j++)
        {
            v.push_back(a[j]);
            if(has(v))
            {
                break;
            }
            ans++;
        }
    }
    cout<<ans<<'\n';
}
main()
{
    //fast;
    int t;
    cin>>t;
    while(t--)
    {
        solve();
    }
}
 
