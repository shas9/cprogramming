#include<bits/stdc++.h>

using namespace std;

class base
{
public:
    void msg()
    {
        cout << "'Msg()' from 'base' class" << endl;
    }
};

class derived: public base
{
public:
    void msg()
    {
        cout << "\'Msg()\' from \'Derived\' class" << endl;
    }
};

int main()
{
    derived d1;

    d1.msg();
}

