/*Create a product class which will take product id show its price by searching with a list of previously stored product and price stored in a file.
  Give message if it is not possible. Use file pr fopen. */

 #include <bits/stdc++.h>

 using namespace std;

 class product
 {
     int product_id;
     int product_price;
     int id;

 public:

     void input()
     {
         cout << "Enter Product ID: ";
         cin >> id;

         FILE *fp;

         fp = fopen("Price.txt", "r");

         int i;
         for(i = 1; i <= 6; i++)
         {
             fscanf(fp, "%d %d", &product_id, &product_price);

             if(product_id == id)
             {
                 cout << "The product id is: " << product_id << endl << "The price is: " << product_price << endl;
                 break;
             }
         }

         if(i == 7)
         {
             cout << "No file found" << endl;
         }


     }
 };

int main()
{
    product a;

    a.input();
}
