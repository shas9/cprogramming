#include<bits/stdc++.h>

using namespace std;

class base
{
public:
    int msg(int x, int y)
    {
        cout << "'Msg()' from 'Base' class" << endl;
        cout << "Sum: " << x + y << endl;
    }
};

class derived1: public base
{
public:
    int msg3(int x, int y)
    {
        cout << "'Msg()' from 'Derived - 1' class" << endl;
        cout << "Sum: " << x + y << endl;
    }


};
class derived2: public derived1
{
public:
    int msg7(int x, int y)
    {
        cout << "'Msg()' from 'Derived-2' class" << endl;
        cout << "Sum: " << x + y << endl;
    }


};

int main()
{
    derived1 *baseptr;

    base b1;
    derived1 d1;
    derived2 d2;

    baseptr = &d2;

    baseptr = &d2;
    baseptr -> msg7(5,6);

    baseptr = &d2;
    baseptr -> msg(5,7);
}


