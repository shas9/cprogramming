/* Add a new  feature to add new products by entering pid and price to that file. */

#include <bits/stdc++.h>

using namespace std;

class product
{
    int i, j, k;
    int product_id, product_price;
    int numberofproduct;
public:

    void getfile()
    {
        FILE *fp;

        fp = fopen("Price-01.txt", "w");
        cout << "Number of Product:";
        cin >> numberofproduct;

        for(i = 1; i <= numberofproduct; i++)
        {
            cin >> product_id >> product_price;

            fprintf(fp, "%d %d\n", product_id, product_price);
        }

        fclose(fp);
    }

    void show()
    {
        FILE *fp1;

        fp1 = fopen("Price-01.txt", "r");

        for(i = 1; i <= numberofproduct; i++)
        {
            fscanf(fp1, "%d %d", &product_id, &product_price);

            cout << "The product id is: " << product_id << endl << "The price is: " << product_price << endl;
        }
    }
};

int main()
{
    product a;

    a.getfile();
    a.show();
}
