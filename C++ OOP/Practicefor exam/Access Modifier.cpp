#include <iostream>

using namespace std;

class base
{
protected:
    int x, y;
public:

    int summ;

    void input()
    {
        cin >> x >> y;
        summ = x + y;
    }

    void display()
    {
        cout << "Based: " << x << " " << y << " " << summ << endl;
    }

};

class der1: public base
{
public:

    virtual void display()
    {
        cout << summ << " " << x << " " << y << endl;
    }
};




int main()
{
    der1 obj1;

    obj1.input();
    obj1.display();

    base obj2;

    obj2.display();
   // obj2.x = 5;
}

