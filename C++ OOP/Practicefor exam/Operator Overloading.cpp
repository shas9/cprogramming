#include <iostream>

using namespace std;

class sum
{
    int x, y;
public:

    sum(int a, int b)
    {
        x = a;
        y = b;
    }

    void display()
    {
        cout << x << " " << y << endl;
    }

    sum operator+(sum m)
    {
        sum temp(5,5);

        temp.x = x + m.x;
        temp.y = y + m.y;

        return temp;
    }

};

int main()
{
    sum e(4,5);
    sum f(6,7);

    sum z = e + f;

    z.display();
}
