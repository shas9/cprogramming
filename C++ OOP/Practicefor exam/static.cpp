#include <iostream>

using namespace std;

class Box {
   public:
      static int objectCount;

      // Constructor definition
      Box(double l = 2, double b = 2.0, double h = 2.0) {
         cout <<"Constructor called." << endl;
         length = l;
         breadth = b;
         height = h;

         // Increase every time object is created
         objectCount++;
      }
      double Volume() {
         cout <<  length * breadth * height << endl;
      }

   private:
      double length;     // Length of a box
      double breadth;    // Breadth of a box
      double height;     // Height of a box
};

// Initialize static member of class Box
int Box::objectCount = 0;

int main(void) {
   Box Box1(3.3, 1.2, 1.5);
   Box1.Volume();    // Declare box1
   Box Box2(8.5, 6.0, 2.0);
   Box2.Volume();
   Box Box3;
   Box3.Volume();    // Declare box2

   // Print total number of objects.
   cout << "Total objects: " << Box::objectCount << endl;
}
