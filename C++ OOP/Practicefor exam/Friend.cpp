#include <iostream>

using namespace std;

class base
{
    int x, y, j;
public:

    base()
    {
        x = 0;
        y = 0;
        j = 5;
    }

    void display()
    {
        cout << x << " " << y << " " << j << endl;
    }

    friend void func(base &l);
};

void func(base &u)
{
    u.x = 7;
    u.y = 8;

    cout << u.x << " " << u.y << " " << u.j << endl;
}




int main()
{
   base g;

   func(g);

   g.display();
}

