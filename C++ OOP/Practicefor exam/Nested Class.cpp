#include <iostream>

using namespace std;

class base
{
public:

    class nested
    {
    public:

        int x;
        int y;

        void disb()
        {
            cout << x << " " << y << endl;
        }
    };

    nested obb;

    void display()
    {
        cout << obb.x << " - " << obb.y << endl;
    }
};


int main()
{
    base obj;

    base :: nested obj2;

    obj.obb.x = 10;
    obj.obb.y = 20;

    obj.display();

    obj2.x =30;
    obj2.y =40;

    obj.display();
    obj.obb.disb();


    obj2.disb();
}
