#include <iostream>

using namespace std;

class base
{
public:

    string *name;
    int age;


    base(string name, int age)
    {
        this -> name = new string(name);
        this -> age = age;
    }

    base(const base &p)
    {
        cout << "COPIED" << endl;

        name = new string(*p.name);
        age = p.age;
    }

    void change(string name, int age)
    {
        *(this -> name) = name;
        this -> age = age;
    }

    void display()
    {
        cout << *name << " - " << age << endl;
    }
};

int main()
{
    base a("SAMIN", 21);

    a.display();

    base b = a;

    b.display();

    a.name = "MITHILA";
    a.age = 20;

    a.display();
    b.display();
}

