#include <iostream>

using namespace std;

int main()
{
    int x, y;

    for(int i = 1; i <= 6; i++)
    {
        cin >> x;

        try
        {
            if(x == 0)
            {
                throw x;
            }

            cout << 5.0/x << endl;
        }

        catch(int f)
        {
            cout << "Error: " << f << endl;
        }
    }
}
