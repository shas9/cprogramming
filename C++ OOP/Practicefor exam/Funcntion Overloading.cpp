#include <iostream>

using namespace std;

class base
{
public:
    virtual void msg()
    {
        cout << "Based one you" << endl;
    }
};

class derived: public base
{
public:
    void msg()
    {
        cout << "Derived on you" << endl;
    }
};

int main()
{
    derived d;
    base b;

    derived *pd;
    base *pb;

//    d.msg();
//    b.msg();

    pb = &d;
    pd = &d;

    pb ->msg();
    pd ->msg();
}
