#include<bits/stdc++.h>

using namespace std;

class base
{
    double a, b, c;
public:
    double msg(double x, double y,double z)
    {
        a = x, b = y, c = z;
        cout << "'Msg()' from 'base' class" << endl;
        cout << "Sum: " << a + b + c << endl;
    }
};

class derived: public base
{
public:
    int msg5(int x, int y)
    {
        cout << "'Msg()' from 'Derived' class" << endl;
        cout << "Sum: " << x + y << endl;
    }


};

int main()
{
    derived d1;

    d1.msg(5.6,6.5,7.5);
}


