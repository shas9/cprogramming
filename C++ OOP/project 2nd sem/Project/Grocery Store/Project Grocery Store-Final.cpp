#include <bits/stdc++.h>`

#define scanlld(longvalue) scanf("%lld", &longvalue)
#define printlld(longvalue) printf("%lld\n", longvalue)

#define scanlf(longvalue) scanf("%lf", &longvalue)
#define printlf(longvalue) printf("%lf\n", longvalue)
#define scanc(letter) scanf("%c", &letter)
#define printc(letter) printf("%c", letter)

#define scans(name) scanf("%s", name)
#define prints(name) printf("%s", name)

#define printnewline printf("\n")

#define ll long long

#define printcase(indexing,ans) printf("Case %lld: %lld\n", indexing, ans)

#define pb(x) push_back(x)

#define bug printf("BUG\n")

using namespace std;

map < string, string > code_name;
map < string, ll > name_taka;

ll totalsell;
ll customer;

const std::string currentDateTime()
{
    time_t     now = time(0);
    struct tm  tstruct;
    char       buf[80];
    tstruct = *localtime(&now);
    strftime(buf, sizeof(buf), "%d-%m-%Y at %X", &tstruct);

    return buf;
}

class store
{
public:

    string name;
    string product_code;
    ll taka;
    ll i, j, k, l, m, n;

    void process()
    {
        ifstream file;
        code_name.clear();
        name_taka.clear();


        file.open("Product.txt");

        while(!file.eof())
        {
            file >> product_code >> name >> taka;

            code_name[product_code] = name;
            name_taka[name] = taka;
        }

        file.close();
    }

    void update()
    {
        cout << "How many product do you want to update: ";
        cin >> k;

        fstream file;

        file.open("Product.txt", ios :: app);

        for(i = 1; i <= k; i++)
        {
            cout << endl;
            cout << "Enter the product code: ";
            cin >> product_code;
            cout << "Type Name: ";
            cin >> name;
            cout << "Enter the Price of " << name << ": ";
            cin >> taka;

            if(code_name[product_code] != "")
            {
                cout << "This code is already exist." << endl;
            }
            else if(name_taka[name] != 0)
            {
                cout << "This product is already updated." << endl;
            }
            else
            {
                code_name[product_code] = name;
                name_taka[name] = taka;

                cout << endl << "Wait for update...." << endl;

                file << endl << product_code << " " << name << " " << taka;

                cout << "Done Successfully." << endl;
            }

            cout << "Press 0 if it is done: ";
            cin >> j;

            if(j == 0)
            {
                break;
            }
        }

        cout << endl;

        file.close();

    }

    void sell()
    {
        ll bill = 0;
        ll quantity;
        ll bought = 0;
        ll total = 0;
        while(1)
        {
            cout << "Press 0 for exit: ";
            cin >> k;

            if(k == 0)
            {
                break;
            }

            cout << endl << "Type Product code: ";
            cin >> product_code;

            if(code_name[product_code] == "")
            {
                cout << "Invalid Code" << endl;
            }
            else
            {
                cout << "The name of the product is " << code_name[product_code] << " with price " << name_taka[code_name[product_code]] << " taka." << endl;
                cout << "Type the amount of the product " << code_name[product_code] << ": ";
                cin >> quantity;

                bill += (quantity * name_taka[code_name[product_code]]);

                bought++;
                total += quantity;
            }


        }

        cout << "Press 1 to confirm: ";
        cin >> k;

        if(k)
        {
            cout << endl << "Type the name of the customer: ";
            cin >> name;
            cout << endl;

            cout << "So the Bill is: " << endl;
            cout << endl;

            cout << "Name: " << name << "." << endl;
            cout << "Product Type: " << bought << "." << endl;
            cout << "Total Item: " << total << "." << endl;
            cout << "The Bill is: " << bill << " taka." << endl;
            cout << "Time: " << currentDateTime() << endl;
            cout << "Thanks for coming." << endl;
            cout << endl;

            totalsell += bill;
            customer++;

        }
    }
}                                                                            ;

int main()
{
    ll i, j, k, l, m, n, o;
    ll testcase;
    ll input, flag, tag;
    totalsell = 0;
    customer = 0;

    store a;

    a.process();

    while(1)
    {
        cout << "Press 1 for Update and 2 for Sell and 3 for exit: ";
        cin >> k;

        if(k == 1)
        {
            a.update();
        }
        else if(k == 2)
        {
            a.sell();
        }
        else if(k == 3)
        {
            break;
        }
        else
        {
            cout << "It doesn't make any sense." << endl;
        }
    }

    cout << endl << "Total sell today: " << totalsell << " taka." << endl << "Customer total came today: " << customer << "." << endl << endl << "Good Night. <3" << endl;

    return 0;


}


